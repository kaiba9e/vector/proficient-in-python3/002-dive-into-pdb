# 深入学习Python调试器

Python调试器(pdb)的功能很强大，这里提供一些辅助工具以帮助快速查阅和使用pdb,
同时存放一些实践Pdb的文档。

## 参考资料
1. [pdb - Python的调试器](https://docs.python.org/zh-cn/3.10/library/pdb.html)
2. [A cheatsheet for the Python Debugger](https://github.com/nblock/pdb-cheatsheet/releases/download/v1.2/pdb-cheatsheet.pdf)
