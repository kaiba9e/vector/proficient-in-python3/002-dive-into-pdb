# Howto Set Value to a Variable

In gdb, we can set value to a variable via:
```
set var      -- Evaluate expression EXP and assign result to variable VAR.
set variable -- Evaluate expression EXP and assign result to variable VAR.
```

**And how can we do the same thing in Pdb?**

**A:** Simple, use `!VAR = ... `

For example,

```python
$ python3 -m pdb ~/.virtualenvs/tmt/bin/tmt lint discover/libraries
> /home/FOODHX/.virtualenvs/tmt/bin/tmt(3)<module>()
-> import click
(Pdb) b tmt/base.py:295
Breakpoint 1 at /home/FOODHX/.virtualenvs/tmt/lib/python3.10/site-packages/tmt/base.py:295
(Pdb) c
/tests/discover/libraries
pass test script must be defined
pass directory path must be absolute
pass directory path must exist
> /home/FOODHX/.virtualenvs/tmt/lib/python3.10/site-packages/tmt/base.py(295)lint_keys()
-> if isinstance(prop_adjust, dict):
(Pdb) ll
289  	    def lint_keys(self, additional_keys):
290  	        """ Return list of invalid keys used, empty when all good """
291  	        known_keys = additional_keys + self._keys
292  	        node_keys = list(self.node.get().keys())
293  	        prop_adjust = self.node.get('adjust')
294  	        if prop_adjust is not None:
295 B->	            if isinstance(prop_adjust, dict):
296  	                node_keys.extend(prop_adjust.keys())
297  	            elif isinstance(prop_adjust, list) or isinstance(prop_adjust, tuple):
298  	                node_keys.extend([key for element in prop_adjust for key in element.keys()])
299  	            else:
300  	                raise tmt.utils.GeneralError(
301  	                    f"Invalid adjust '{prop_adjust}'.")
302  	        return [key for key in node_keys if key not in known_keys]
(Pdb) p known_keys, node_keys
(['extra-nitrate', 'extra-hardware', 'extra-pepa', 'extra-summary', 'extra-task', 'relevancy', 'coverage', 'adjust', 'summary', 'description', 'contact', 'component', 'test', 'path', 'framework', 'manual', 'require', 'recommend', 'environment', 'duration', 'enabled', 'order', 'result', 'tag', 'tier', 'link'], ['contact', 'framework', 'require', 'tag', 'test', 'tier', 'adjust', 'description', 'duration', 'enabled', 'summary'])
(Pdb)
(Pdb) ########################## 对变量set01/set02进行赋值 ########################################
(Pdb) !set01 = set(known_keys)
(Pdb) !set02 = set(node_keys)
(Pdb)
(Pdb) ########################## 求两个集合的交集 #################################################
(Pdb) set02 & set01
{'tier', 'require', 'tag', 'enabled', 'description', 'summary', 'duration', 'adjust', 'test', 'framework', 'contact'}
(Pdb)
(Pdb)
(Pdb) ########################## 求两个集合的并集 #################################################
(Pdb) set02 | set01
{'extra-nitrate', 'extra-hardware', 'framework', 'extra-pepa', 'manual', 'summary', 'duration', 'tier', 'require', 'extra-task', 'recommend', 'result', 'description', 'component', 'extra-summary', 'coverage', 'adjust', 'path', 'environment', 'tag', 'enabled', 'link', 'test', 'contact', 'order', 'relevancy'}
(Pdb)
(Pdb)
(Pdb) ########################## 求两个集合的差集 #################################################
(Pdb) ################### A - B: = A - (A & B) : 在集合A中但是不在集合B中的所有元素
(Pdb) ################### e.g. A = {1, 2, 3},
(Pdb) ###################      B = {3, 4, 5},
(Pdb) ###################      A - B = {1, 2}         # A & B = {3}, A = {1, 2, 3}
(Pdb) set02 - set01
set()
(Pdb) ########################## 求两个集合的对称差集 #############################################
(Pdb) ################### A ^ B: (A | B) - (A & B)
(Pdb) ################### e.g. A = {1, 2, 3},
(Pdb) ###################      B = {3, 4, 5},
(Pdb) ###################      A - B = {1, 2, 4, 5}   # A & B = {3}, A | B = {1, 2, 3, 4, 5}
(Pdb) set02 ^ set01
{'extra-task', 'recommend', 'result', 'extra-nitrate', 'component', 'extra-summary', 'coverage', 'path', 'extra-hardware', 'extra-pepa', 'manual', 'environment', 'link', 'order', 'relevancy'}
(Pdb)
(Pdb) #############################################################################################
(Pdb) # 302  	        return [key for key in node_keys if key not in known_keys]                #
(Pdb) # XXX: L302 can be:                                                                         #
(Pdb) #                 return list(set(node_keys) - set(known_keys))                             #
(Pdb) #############################################################################################
(Pdb)
```
