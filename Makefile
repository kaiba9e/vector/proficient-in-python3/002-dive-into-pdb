TARGETS = pdbman

all: $(TARGETS)

%: %.py
	cp $< $@
	chmod +x $@

check:
	icat -c *.py

clean:
	rm -rf __pycache__
clobber: clean
	rm -f $(TARGETS)
cl: clobber
