#!/usr/bin/python3
# coding: utf-8

"""
pdbman - Helper utility to practice pdb.

Version : 1.1
Date    : 2022/07/12
Author  : Vector Li <huanli@redhat.com>
"""

import sys
import os
import getopt


CstrNONE    = 0
CstrGRAY    = 30
CstrRED     = 31
CstrGREEN   = 32
CstrYELLOW  = 33
CstrBLUE    = 34
CstrMAGENTA = 35
CstrCYAN    = 36
CstrWHITE   = 37
class Cstr(object):
    """
    Cstr(object) -> Colorful string

        o none    : CstrNONE      :  0 : "%s" % str
        o gray    : CstrGRAY      : 30 : "\033[1;30m%s\033[m" % str
        o red     : CstrRED       : 31 : "\033[1;31m%s\033[m" % str
        o green   : CstrGREEN     : 32 : "\033[1;32m%s\033[m" % str
        o yellow  : CstrYELLOW    : 33 : "\033[1;33m%s\033[m" % str
        o blue    : CstrBLUE      : 34 : "\033[1;34m%s\033[m" % str
        o magenta : CstrMAGENTA   : 35 : "\033[1;35m%s\033[m" % str
        o cyan    : CstrCYAN      : 36 : "\033[1;36m%s\033[m" % str
        o white   : CstrWHITE     : 37 : "\033[1;37m%s\033[m" % str

        Return a colorful string representation of the object.

        Example:
        (1) print Cstr("Hello World", CstrRED)
        (2) print Cstr("Hello World")
        (3) print Cstr()
    """

    def __init__(self, s="", color=CstrNONE):
        self.color = color
        self.tstr  = s
        self.cstr  = self._tocstr()

    def __str__(self):
        return self.cstr

    def __len__(self):
        return len(self.tstr)

    def _tocstr(self):
        if self.color == CstrNONE:
            return self.tstr

        if not self._isatty():
            return self.tstr
        return "\033[1;%dm%s\033[m" % (self.color, self.tstr)

    def _isatty(self):
        import os
        s = os.getenv("ISATTY")
        if s is None:
            s = ""

        if s.upper() == "YES":
            return True

        if s.upper() == "NO":
            return False

        if sys.stdout.isatty() and sys.stderr.isatty():
            return True
        return False


class Pdbman(object):
    """ Pdb MAN """

    def __init__(self):
        self._docs = None
        self._cheatsheets = None

        self._set_docs()
        self._set_cheatsheets()

    def _set_docs(self):
        docs = list()
        docs.append({
            'desc': 'official',
            'url': 'https://docs.python.org/zh-cn/3.10/library/pdb.html'
            })
        docs.append({
            'desc': 'cheatsheet',
            'url': 'https://github.com/nblock/pdb-cheatsheet/releases/download/v1.2/pdb-cheatsheet.pdf'
            })
        self._docs = docs

    def output_docs(self):
        print(Cstr("\tPDB Documents", CstrYELLOW))
        print(Cstr("\t=============", CstrGRAY))
        for doc in self._docs:
            print("\t* %s:\t%s" % (Cstr(doc['desc'].title(), CstrBLUE), doc['url']))

    def _set_cheatsheets(self):
        """
        https://github.com/nblock/pdb-cheatsheet/blob/master/pdb-cheatsheet.tex
        """

        csts = list()

        #######################################################################
        csts_01 = dict()
        csts_01['alias'] = 'gs'
        csts_01['title'] = 'Getting started'
        csts_01['body'] = list()
        csts_01['body'].append({
            'cmd': 'import pdb; pdb.set_trace()',
            'desc_eng': 'start pdb from within a script',
            'desc_chs': '从Python脚本内部启动pdb'
            })
        csts_01['body'].append({
            'cmd': 'python -m pdb <file.py>',
            'desc_eng': 'start pdb from the command line',
            'desc_chs': '从命令行启动pdb'
            })

        csts.append(csts_01)

        #######################################################################
        csts_02 = dict()
        csts_02['alias'] = 'bc'
        csts_02['title'] = 'Basics'
        csts_02['body'] = list()
        csts_02['body'].append({
            'cmd': 'h(elp)',
            'desc_eng': 'print available commands',
            'desc_chs': '打印所有可用的命令'
            })
        csts_02['body'].append({
            'cmd': 'h(elp) command',
            'desc_eng': 'print help about command',
            'desc_chs': '打印某个命令的详细帮助'
            })
        csts_02['body'].append({
            'cmd': 'q(quit)',
            'desc_eng': 'quit debugger',
            'desc_chs': '退出调试器'
            })
        csts.append(csts_02)

        #######################################################################
        csts_03 = dict()
        csts_03['alias'] = 'ex'
        csts_03['title'] = 'Examine'
        csts_03['body'] = list()
        csts_03['body'].append({
            'cmd': 'p(rint) expr',
            'desc_eng': 'print the value of {expr}',
            'desc_chs': '打印表达式的值'
            })
        csts_03['body'].append({
            'cmd': 'pp expr',
            'desc_eng': 'pretty-print the value of {expr}',
            'desc_chs': '优雅地打印表达式的值'
            })
        csts_03['body'].append({
            'cmd': 'w(here)',
            'desc_eng': 'print current position (including stack trace)',
            'desc_chs': '打印当前的位置（包括调用栈）'
            })
        csts_03['body'].append({
            'cmd': 'l(ist)',
            'desc_eng': 'list 11 lines of code around the current line',
            'desc_chs': '列出当前行附近的11行代码'
            })
        csts_03['body'].append({
            'cmd': 'l(ist) first, last',
            'desc_eng': 'list from first to last line number',
            'desc_chs': '列出从first到last的N行代码'
            })
        csts_03['body'].append({
            'cmd': 'a(rgs)',
            'desc_eng': 'print the args of the current function',
            'desc_chs': '打印当前函数的参数'
            })
        csts.append(csts_03)

        #######################################################################
        csts_04 = dict()
        csts_04['alias'] = 'mv'
        csts_04['title'] = 'Movement'
        csts_04['body'] = list()
        csts_04['body'].append({
            'cmd': '<ENTER>',
            'desc_eng': 'repeat the last command',
            'desc_chs': '重复上一条命令'
            })
        csts_04['body'].append({
            'cmd': 'n(ext)',
            'desc_eng': 'execute the current statement (step over)',
            'desc_chs': '执行当前语句（单步执行）'
            })
        csts_04['body'].append({
            'cmd': 's(tep)',
            'desc_eng': 'execute and step into function',
            'desc_chs': '执行并单步进入一个函数'
            })
        csts_04['body'].append({
            'cmd': 'r(eturn)',
            'desc_eng': 'continue execution until the current function returns',
            'desc_chs': '继续执行直到当前函数返回'
            })
        csts_04['body'].append({
            'cmd': 'c(ontinue)',
            'desc_eng': 'continue execution until a breakpoint is encountered',
            'desc_chs': '继续执行直到遇到一个断点'
            })
        csts_04['body'].append({
            'cmd': 'until',
            'desc_eng': 'continue execution until the end of a loop or until the set line',
            'desc_chs': '继续执行直到循环结束或设置行结束'
            })
        csts_04['body'].append({
            'cmd': 'j(ump)',
            'desc_eng': 'set the next line that will be executed (local frame only)',
            'desc_chs': '设置要执行的下一行（仅限本地帧）'
            })
        csts_04['body'].append({
            'cmd': 'u(p)',
            'desc_eng': 'move one level up in the stack trace',
            'desc_chs': '在栈轨迹中向上移一帧'
            })
        csts_04['body'].append({
            'cmd': 'd(own)',
            'desc_eng': 'move one level down in the stack trace',
            'desc_chs': '在栈轨迹中向下移一帧'
            })
        csts.append(csts_04)

        #######################################################################
        csts_05 = dict()
        csts_05['alias'] = 'bp'
        csts_05['title'] = 'Breakpoints'
        csts_05['body'] = list()
        csts_05['body'].append({
            'cmd': 'b(reak)',
            'desc_eng': 'show all breakpoints with its number',
            'desc_chs': '显示所有断点及其编号'
            })
        csts_05['body'].append({
            'cmd': 'b(reak) lineno',
            'desc_eng': 'set a breakpoint at lineno',
            'desc_chs': '在某行设置断点'
            })
        csts_05['body'].append({
            'cmd': 'b(reak) lineno, cond',
            'desc_eng': 'stop at breakpoint lineno if Python condition cond holds, e.g. i == 42',
            'desc_chs': '设置条件断点'
            })
        csts_05['body'].append({
            'cmd': 'b(reak) file:lineno',
            'desc_eng': 'set a breakpoint in file at lineno',
            'desc_chs': '在某个文件的某行设置断点'
            })
        csts_05['body'].append({
            'cmd': 'b(reak) func',
            'desc_eng': 'set a breakpoint at the first line of a func',
            'desc_chs': '在某个函数的首行设置断点'
            })
        csts_05['body'].append({
            'cmd': 'tbreak lineno',
            'desc_eng': 'set a temporary breakpoint at lineno, i.e. is removed when first hit',
            'desc_chs': '在某行设置临时断点，断点一旦命中就被移除'
            })
        csts_05['body'].append({
            'cmd': 'disable number',
            'desc_eng': 'disable breakpoint number',
            'desc_chs': '禁用断点编号'
            })
        csts_05['body'].append({
            'cmd': 'enable number',
            'desc_eng': 'enable breakpoint number',
            'desc_chs': '启用断点编号'
            })
        csts_05['body'].append({
            'cmd': 'clear number',
            'desc_eng': 'delete breakpoint number',
            'desc_chs': '删除断点编号'
            })
        csts.append(csts_05)

        #######################################################################
        csts_06 = dict()
        csts_06['alias'] = 'mc'
        csts_06['title'] = 'Miscellaneous'
        csts_06['body'] = list()
        csts_06['body'].append({
            'cmd': '!stmt',
            'desc_eng': 'treat stmt as a Python statement instead of a pdb command',
            'desc_chs': '将stmt视为Python声明而不是pdb命令'
            })
        csts_06['body'].append({
            'cmd': 'alias map stmt',
            'desc_eng': 'map Python statement as a map command',
            'desc_chs': '将Python声明映射为命令'
            })
        csts_06['body'].append({
            'cmd': 'alias map <arg1 ...> stmt',
            'desc_eng': 'pass arguments to Python statement. stmt includes %1, %2, ... literals',
            'desc_chs': '将参数传递给Python声明'
            })
        csts.append(csts_06)

        self._cheatsheets = csts


    def _get_cheatsheets_max_sz(self):
        max_cmd_sz = 0
        max_desc_eng_sz = 0
        for cst in self._cheatsheets:
            for entry in cst['body']:
                cmd_sz = len(entry['cmd'])
                if cmd_sz > max_cmd_sz:
                    max_cmd_sz = cmd_sz
                desc_eng_sz = len(entry['desc_eng'])
                if desc_eng_sz > max_desc_eng_sz:
                    max_desc_eng_sz = desc_eng_sz
        return (max_cmd_sz, max_desc_eng_sz)


    def _output_cheatsheets(self, obj, max_cmd_sz, max_desc_eng_sz):
        print("")
        print("\t%s" % Cstr(obj['title'], CstrYELLOW))
        print("\t%s" % Cstr('=' * len(obj['title']), CstrGRAY))
        for entry in obj['body']:
            cmd = Cstr(entry['cmd'], CstrCYAN)
            nspaces1 = ' ' * (max_cmd_sz - len(entry['cmd']))
            nspaces2 = ' ' * (max_desc_eng_sz - len(entry['desc_eng']))
            desc_eng = entry['desc_eng']
            desc_chs = Cstr(entry['desc_chs'], CstrMAGENTA)
            print("\to %s%s | %s%s | %s" % (cmd, nspaces1, desc_eng, nspaces2, desc_chs))
        print("")


    def output_cheatsheets(self, alias=None):
        max_cmd_sz, max_desc_eng_sz = self._get_cheatsheets_max_sz()

        if alias is None:
            csts = [cst for cst in self._cheatsheets]
        else:
            csts = []
            for cst in self._cheatsheets:
                if cst['alias'] != alias:
                    continue
                csts.append(cst)

        for cst in csts:
            self._output_cheatsheets(cst, max_cmd_sz, max_desc_eng_sz)


def usage(s):
    s = Cstr(s, CstrYELLOW)
    print("Usage: %s <-d|-a|-s|-c|-x|-m|-b|-M|-h>" % s, file=sys.stderr)
    print("e.g.", file=sys.stderr)
    print("       %s -d # doc" % s, file=sys.stderr)
    print("       %s -a # all" % s, file=sys.stderr)
    print("       %s -s # getting started" % s, file=sys.stderr)
    print("       %s -c # basics" % s, file=sys.stderr)
    print("       %s -x # examine" % s, file=sys.stderr)
    print("       %s -m # movement" % s, file=sys.stderr)
    print("       %s -b # breakpoints" % s, file=sys.stderr)
    print("       %s -M # miscellaneous" % s, file=sys.stderr)


def main(argc, argv):
    pm = Pdbman()
    options, rargv = getopt.getopt(argv[1:], ":dascxmbMh",
                                   ["doc",
                                    "all",
                                    "start",
                                    "basic",
                                    "examine",
                                    "move",
                                    "break",
                                    "misc",
                                    "help"])
    for opt, arg in options:
        if opt in ("-d", "--doc"):
            pm.output_docs()
            return 0
        elif opt in ("-a", "--all"):
            pm.output_cheatsheets()
            return 0
        elif opt in ("-s", "--start"):
            pm.output_cheatsheets('gs')
            return 0
        elif opt in ("-c", "--basic"):
            pm.output_cheatsheets('bc')
            return 0
        elif opt in ("-x", "--examine"):
            pm.output_cheatsheets('ex')
            return 0
        elif opt in ("-m", "--move"):
            pm.output_cheatsheets('mv')
            return 0
        elif opt in ("-b", "--break"):
            pm.output_cheatsheets('bp')
            return 0
        elif opt in ("-M", "--misc"):
            pm.output_cheatsheets('mc')
            return 0
        elif opt in ("-h", "--help"):
            usage(argv[0])
            return 1
        else:
            return 1

    if not rargv:
        usage(argv[0])
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
